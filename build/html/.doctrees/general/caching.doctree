�cdocutils.nodes
document
q)�q}q(U	nametypesq}q(X   web page cachingqNX   deleting cachesqNX   how does caching work?qNX   enabling cachingq	NuUsubstitution_defsq
}qUparse_messagesq]qUcurrent_sourceqNU
decorationqNUautofootnote_startqKUnameidsq}q(hUweb-page-cachingqhUdeleting-cachesqhUhow-does-caching-workqh	Uenabling-cachingquUchildrenq]qcdocutils.nodes
section
q)�q}q(U	rawsourceqU UparentqhUsourceqXQ   E:\developer_server\domains\ricms.local\user_guide_src\source\general\caching.rstqUtagnameq Usectionq!U
attributesq"}q#(Udupnamesq$]Uclassesq%]Ubackrefsq&]Uidsq']q(haUnamesq)]q*hauUlineq+KUdocumentq,hh]q-(cdocutils.nodes
title
q.)�q/}q0(hX   Web Page Cachingq1hhhhh Utitleq2h"}q3(h$]h%]h&]h']h)]uh+Kh,hh]q4cdocutils.nodes
Text
q5X   Web Page Cachingq6��q7}q8(hh1hh/ubaubcdocutils.nodes
paragraph
q9)�q:}q;(hXN   CodeIgniter lets you cache your pages in order to achieve maximum
performance.q<hhhhh U	paragraphq=h"}q>(h$]h%]h&]h']h)]uh+Kh,hh]q?h5XN   CodeIgniter lets you cache your pages in order to achieve maximum
performance.q@��qA}qB(hh<hh:ubaubh9)�qC}qD(hXl  Although CodeIgniter is quite fast, the amount of dynamic information
you display in your pages will correlate directly to the server
resources, memory, and processing cycles utilized, which affect your
page load speeds. By caching your pages, since they are saved in their
fully rendered state, you can achieve performance much closer to that of
static web pages.qEhhhhh h=h"}qF(h$]h%]h&]h']h)]uh+Kh,hh]qGh5Xl  Although CodeIgniter is quite fast, the amount of dynamic information
you display in your pages will correlate directly to the server
resources, memory, and processing cycles utilized, which affect your
page load speeds. By caching your pages, since they are saved in their
fully rendered state, you can achieve performance much closer to that of
static web pages.qH��qI}qJ(hhEhhCubaubh)�qK}qL(hU hhhhh h!h"}qM(h$]h%]h&]h']qNhah)]qOhauh+Kh,hh]qP(h.)�qQ}qR(hX   How Does Caching Work?qShhKhhh h2h"}qT(h$]h%]h&]h']h)]uh+Kh,hh]qUh5X   How Does Caching Work?qV��qW}qX(hhShhQubaubh9)�qY}qZ(hX�  Caching can be enabled on a per-page basis, and you can set the length
of time that a page should remain cached before being refreshed. When a
page is loaded for the first time, the file will be cached using the
currently configured cache engine. On subsequent page loads the cache file
will be retrieved and sent to the requesting user's browser. If it has
expired, it will be deleted and refreshed before being sent to the
browser.q[hhKhhh h=h"}q\(h$]h%]h&]h']h)]uh+Kh,hh]q]h5X�  Caching can be enabled on a per-page basis, and you can set the length
of time that a page should remain cached before being refreshed. When a
page is loaded for the first time, the file will be cached using the
currently configured cache engine. On subsequent page loads the cache file
will be retrieved and sent to the requesting user's browser. If it has
expired, it will be deleted and refreshed before being sent to the
browser.q^��q_}q`(hh[hhYubaubcdocutils.nodes
comment
qa)�qb}qc(hXi   note: The Benchmark tag is not cached so you can still view your page
load speed when caching is enabled.hhKhhh Ucommentqdh"}qe(U	xml:spaceqfUpreserveqgh']h&]h$]h%]h)]uh+Kh,hh]qhh5Xi   note: The Benchmark tag is not cached so you can still view your page
load speed when caching is enabled.qi��qj}qk(hU hhbubaubeubh)�ql}qm(hU hhhhh h!h"}qn(h$]h%]h&]h']qohah)]qph	auh+Kh,hh]qq(h.)�qr}qs(hX   Enabling Cachingqthhlhhh h2h"}qu(h$]h%]h&]h']h)]uh+Kh,hh]qvh5X   Enabling Cachingqw��qx}qy(hhthhrubaubh9)�qz}q{(hXL   To enable caching, put the following tag in any of your controller
methods::hhlhhh h=h"}q|(h$]h%]h&]h']h)]uh+K h,hh]q}h5XK   To enable caching, put the following tag in any of your controller
methods:q~��q}q�(hXK   To enable caching, put the following tag in any of your controller
methods:hhzubaubcdocutils.nodes
literal_block
q�)�q�}q�(hX   $this->cachePage($n);hhlhhh Uliteral_blockq�h"}q�(hfhgh']h&]h$]h%]h)]uh+K#h,hh]q�h5X   $this->cachePage($n);q���q�}q�(hU hh�ubaubh9)�q�}q�(hX_   Where ``$n`` is the number of **seconds** you wish the page to remain
cached between refreshes.hhlhhh h=h"}q�(h$]h%]h&]h']h)]uh+K%h,hh]q�(h5X   Where q���q�}q�(hX   Where hh�ubcdocutils.nodes
literal
q�)�q�}q�(hX   ``$n``h"}q�(h$]h%]h&]h']h)]uhh�h]q�h5X   $nq���q�}q�(hU hh�ubah Uliteralq�ubh5X    is the number of q���q�}q�(hX    is the number of hh�ubcdocutils.nodes
strong
q�)�q�}q�(hX   **seconds**h"}q�(h$]h%]h&]h']h)]uhh�h]q�h5X   secondsq���q�}q�(hU hh�ubah Ustrongq�ubh5X6    you wish the page to remain
cached between refreshes.q���q�}q�(hX6    you wish the page to remain
cached between refreshes.hh�ubeubh9)�q�}q�(hX�   The above tag can go anywhere within a method. It is not affected by
the order that it appears, so place it wherever it seems most logical to
you. Once the tag is in place, your pages will begin being cached.q�hhlhhh h=h"}q�(h$]h%]h&]h']h)]uh+K(h,hh]q�h5X�   The above tag can go anywhere within a method. It is not affected by
the order that it appears, so place it wherever it seems most logical to
you. Once the tag is in place, your pages will begin being cached.q���q�}q�(hh�hh�ubaubcdocutils.nodes
important
q�)�q�}q�(hXp   If you change configuration options that might affect
your output, you have to manually delete your cache files.hhlhhh U	importantq�h"}q�(h$]h%]h&]h']h)]uh+Nh,hh]q�h9)�q�}q�(hXp   If you change configuration options that might affect
your output, you have to manually delete your cache files.q�hh�hhh h=h"}q�(h$]h%]h&]h']h)]uh+K,h]q�h5Xp   If you change configuration options that might affect
your output, you have to manually delete your cache files.q���q�}q�(hh�hh�ubaubaubcdocutils.nodes
note
q�)�q�}q�(hXs   Before the cache files can be written you must set the cache
engine up by editing **application/Config/Cache.php**.hhlhhh Unoteq�h"}q�(h$]h%]h&]h']h)]uh+Nh,hh]q�h9)�q�}q�(hXs   Before the cache files can be written you must set the cache
engine up by editing **application/Config/Cache.php**.hh�hhh h=h"}q�(h$]h%]h&]h']h)]uh+K/h]q�(h5XR   Before the cache files can be written you must set the cache
engine up by editing qɅ�q�}q�(hXR   Before the cache files can be written you must set the cache
engine up by editing hh�ubh�)�q�}q�(hX    **application/Config/Cache.php**h"}q�(h$]h%]h&]h']h)]uhh�h]q�h5X   application/Config/Cache.phpqЅ�q�}q�(hU hh�ubah h�ubh5X   .��q�}q�(hX   .hh�ubeubaubeubh)�q�}q�(hU hhhhh h!h"}q�(h$]h%]h&]h']q�hah)]q�hauh+K3h,hh]q�(h.)�q�}q�(hX   Deleting Cachesq�hh�hhh h2h"}q�(h$]h%]h&]h']h)]uh+K3h,hh]q�h5X   Deleting Cachesq���q�}q�(hh�hh�ubaubh9)�q�}q�(hXx   If you no longer wish to cache a file you can remove the caching tag and
it will no longer be refreshed when it expires.q�hh�hhh h=h"}q�(h$]h%]h&]h']h)]uh+K5h,hh]q�h5Xx   If you no longer wish to cache a file you can remove the caching tag and
it will no longer be refreshed when it expires.q腁q�}q�(hh�hh�ubaubh�)�q�}q�(hXX   Removing the tag will not delete the cache immediately. It will
have to expire normally.hh�hhh h�h"}q�(h$]h%]h&]h']h)]uh+Nh,hh]q�h9)�q�}q�(hXX   Removing the tag will not delete the cache immediately. It will
have to expire normally.q�hh�hhh h=h"}q�(h$]h%]h&]h']h)]uh+K8h]q�h5XX   Removing the tag will not delete the cache immediately. It will
have to expire normally.q�q�}q�(hh�hh�ubaubaubeubeubahU Utransformerq�NUfootnote_refsq�}q�Urefnamesq�}q�Usymbol_footnotesq�]q�Uautofootnote_refsq�]q�Usymbol_footnote_refsr   ]r  U	citationsr  ]r  h,hUcurrent_liner  NUtransform_messagesr  ]r  Ureporterr  NUid_startr  KUautofootnotesr	  ]r
  Ucitation_refsr  }r  Uindirect_targetsr  ]r  Usettingsr  (cdocutils.frontend
Values
r  or  }r  (Ufootnote_backlinksr  KUrecord_dependenciesr  NUrfc_base_urlr  Uhttps://tools.ietf.org/html/r  U	tracebackr  �Upep_referencesr  NUstrip_commentsr  NUtoc_backlinksr  Uentryr  Ulanguage_coder  Uenr  U	datestampr  NUreport_levelr  KU_destinationr   NU
halt_levelr!  KUstrip_classesr"  Nh2NUerror_encoding_error_handlerr#  Ubackslashreplacer$  Udebugr%  NUembed_stylesheetr&  �Uoutput_encoding_error_handlerr'  Ustrictr(  Usectnum_xformr)  KUdump_transformsr*  NUdocinfo_xformr+  KUwarning_streamr,  NUpep_file_url_templater-  Upep-%04dr.  Uexit_status_levelr/  KUconfigr0  NUstrict_visitorr1  NUcloak_email_addressesr2  �Utrim_footnote_reference_spacer3  �Uenvr4  NUdump_pseudo_xmlr5  NUexpose_internalsr6  NUsectsubtitle_xformr7  �Usource_linkr8  NUrfc_referencesr9  NUoutput_encodingr:  Uutf-8r;  U
source_urlr<  NUinput_encodingr=  U	utf-8-sigr>  U_disable_configr?  NU	id_prefixr@  U Ucharacter_level_inline_markuprA  �U	tab_widthrB  KUerror_encodingrC  Ucp866rD  U_sourcerE  hUgettext_compactrF  �U	generatorrG  NUdump_internalsrH  NUsmart_quotesrI  �Upep_base_urlrJ  U https://www.python.org/dev/peps/rK  Usyntax_highlightrL  UlongrM  Uinput_encoding_error_handlerrN  j(  Uauto_id_prefixrO  UidrP  Udoctitle_xformrQ  �Ustrip_elements_with_classesrR  NU_config_filesrS  ]rT  Ufile_insertion_enabledrU  �Uraw_enabledrV  KUdump_settingsrW  NubUsymbol_footnote_startrX  K h'}rY  (hh�hhhhKhhluUsubstitution_namesrZ  }r[  h h,h"}r\  (h$]h']h&]Usourcehh%]h)]uU	footnotesr]  ]r^  Urefidsr_  }r`  ub.