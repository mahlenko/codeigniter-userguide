�cdocutils.nodes
document
q)�q}q(U	nametypesq}q(X   modelsqNX   the componentsqNX   models, views, and controllersqNX   controllersq	NX   viewsq
NuUsubstitution_defsq}qUparse_messagesq]qUcurrent_sourceqNU
decorationqNUautofootnote_startqKUnameidsq}q(hUmodelsqhUthe-componentsqhUmodels-views-and-controllersqh	Ucontrollersqh
UviewsquUchildrenq]qcdocutils.nodes
section
q)�q}q(U	rawsourceqU UparentqhUsourceq XN   E:\developer_server\domains\ricms.local\user_guide_src\source\concepts\mvc.rstq!Utagnameq"Usectionq#U
attributesq$}q%(Udupnamesq&]Uclassesq']Ubackrefsq(]Uidsq)]q*haUnamesq+]q,hauUlineq-KUdocumentq.hh]q/(cdocutils.nodes
title
q0)�q1}q2(hX   Models, Views, and Controllersq3hhh h!h"Utitleq4h$}q5(h&]h']h(]h)]h+]uh-Kh.hh]q6cdocutils.nodes
Text
q7X   Models, Views, and Controllersq8��q9}q:(hh3hh1ubaubcdocutils.nodes
paragraph
q;)�q<}q=(hXA  Whenever you create an application, you have to find a way to organize the code to make it simple to locate
the proper files and make it simple to maintain. Like most of the web frameworks, CodeIgniter uses the Model,
View, Controller (MVC) pattern to organize the files. This keeps the data, the presentation, and flow through the
application as separate parts. It should be noted that there are many views on the exact roles of each element,
but this document describes our take on it. If you think of it differently, you're free to modify how you use
each piece as you need.q>hhh h!h"U	paragraphq?h$}q@(h&]h']h(]h)]h+]uh-Kh.hh]qAh7XA  Whenever you create an application, you have to find a way to organize the code to make it simple to locate
the proper files and make it simple to maintain. Like most of the web frameworks, CodeIgniter uses the Model,
View, Controller (MVC) pattern to organize the files. This keeps the data, the presentation, and flow through the
application as separate parts. It should be noted that there are many views on the exact roles of each element,
but this document describes our take on it. If you think of it differently, you're free to modify how you use
each piece as you need.qB��qC}qD(hh>hh<ubaubh;)�qE}qF(hXy   **Models** manage the data of the application, and help to enforce any special business rules the application might need.qGhhh h!h"h?h$}qH(h&]h']h(]h)]h+]uh-Kh.hh]qI(cdocutils.nodes
strong
qJ)�qK}qL(hX
   **Models**h$}qM(h&]h']h(]h)]h+]uhhEh]qNh7X   ModelsqO��qP}qQ(hU hhKubah"UstrongqRubh7Xo    manage the data of the application, and help to enforce any special business rules the application might need.qS��qT}qU(hXo    manage the data of the application, and help to enforce any special business rules the application might need.hhEubeubh;)�qV}qW(hX^   **Views** are simple files, with little to no logic, that display the information to the user.qXhhh h!h"h?h$}qY(h&]h']h(]h)]h+]uh-Kh.hh]qZ(hJ)�q[}q\(hX	   **Views**h$}q](h&]h']h(]h)]h+]uhhVh]q^h7X   Viewsq_��q`}qa(hU hh[ubah"hRubh7XU    are simple files, with little to no logic, that display the information to the user.qb��qc}qd(hXU    are simple files, with little to no logic, that display the information to the user.hhVubeubh;)�qe}qf(hX�   **Controllers** act as glue code, marshalling data back and forth between the view (or the user that's seeing it) and
the data storage.hhh h!h"h?h$}qg(h&]h']h(]h)]h+]uh-Kh.hh]qh(hJ)�qi}qj(hX   **Controllers**h$}qk(h&]h']h(]h)]h+]uhheh]qlh7X   Controllersqm��qn}qo(hU hhiubah"hRubh7Xx    act as glue code, marshalling data back and forth between the view (or the user that's seeing it) and
the data storage.qp��qq}qr(hXx    act as glue code, marshalling data back and forth between the view (or the user that's seeing it) and
the data storage.hheubeubh;)�qs}qt(hX�  At their most basic, controllers and models are simply classes that have a specific job. They are not the only class
types that you can use, obviously, but the make up the core of how this framework is designed to be used. They even
have designated directories in the **/application** directory for their storage, though you're free to store them
wherever you desire, as long as they are properly namespaced. We will discuss that in more detail below.hhh h!h"h?h$}qu(h&]h']h(]h)]h+]uh-Kh.hh]qv(h7X  At their most basic, controllers and models are simply classes that have a specific job. They are not the only class
types that you can use, obviously, but the make up the core of how this framework is designed to be used. They even
have designated directories in the qw��qx}qy(hX  At their most basic, controllers and models are simply classes that have a specific job. They are not the only class
types that you can use, obviously, but the make up the core of how this framework is designed to be used. They even
have designated directories in the hhsubhJ)�qz}q{(hX   **/application**h$}q|(h&]h']h(]h)]h+]uhhsh]q}h7X   /applicationq~��q}q�(hU hhzubah"hRubh7X�    directory for their storage, though you're free to store them
wherever you desire, as long as they are properly namespaced. We will discuss that in more detail below.q���q�}q�(hX�    directory for their storage, though you're free to store them
wherever you desire, as long as they are properly namespaced. We will discuss that in more detail below.hhsubeubh;)�q�}q�(hX@   Let's take a closer look at each of these three main components.q�hhh h!h"h?h$}q�(h&]h']h(]h)]h+]uh-Kh.hh]q�h7X@   Let's take a closer look at each of these three main components.q���q�}q�(hh�hh�ubaubh)�q�}q�(hU hhh h!h"h#h$}q�(h&]h']h(]h)]q�hah+]q�hauh-Kh.hh]q�(h0)�q�}q�(hX   The Componentsq�hh�h h!h"h4h$}q�(h&]h']h(]h)]h+]uh-Kh.hh]q�h7X   The Componentsq���q�}q�(hh�hh�ubaubh)�q�}q�(hU hh�h h!h"h#h$}q�(h&]h']h(]h)]q�hah+]q�h
auh-K h.hh]q�(h0)�q�}q�(hX   Viewsq�hh�h h!h"h4h$}q�(h&]h']h(]h)]h+]uh-K h.hh]q�h7X   Viewsq���q�}q�(hh�hh�ubaubh;)�q�}q�(hX�   Views are the simplest files and are typically HTML with very small amounts of PHP. The PHP should be very simple,
usually just displaying a variable's contents, or looping over some items and displaying their information in a table.q�hh�h h!h"h?h$}q�(h&]h']h(]h)]h+]uh-K"h.hh]q�h7X�   Views are the simplest files and are typically HTML with very small amounts of PHP. The PHP should be very simple,
usually just displaying a variable's contents, or looping over some items and displaying their information in a table.q���q�}q�(hh�hh�ubaubh;)�q�}q�(hX  Views get the data to display from the controllers, who pass it to the views as variables that can be displayed
with simple ``echo`` calls. You can also display other views within a view, making it pretty simple to display a
common header or footer on every page.hh�h h!h"h?h$}q�(h&]h']h(]h)]h+]uh-K%h.hh]q�(h7X|   Views get the data to display from the controllers, who pass it to the views as variables that can be displayed
with simple q���q�}q�(hX|   Views get the data to display from the controllers, who pass it to the views as variables that can be displayed
with simple hh�ubcdocutils.nodes
literal
q�)�q�}q�(hX   ``echo``h$}q�(h&]h']h(]h)]h+]uhh�h]q�h7X   echoq���q�}q�(hU hh�ubah"Uliteralq�ubh7X�    calls. You can also display other views within a view, making it pretty simple to display a
common header or footer on every page.q���q�}q�(hX�    calls. You can also display other views within a view, making it pretty simple to display a
common header or footer on every page.hh�ubeubh;)�q�}q�(hX3  Views are generally stored in **/application/Views**, but can quickly become unwieldy if not organized in some fashion.
CodeIgniter does not enforce any type of organization, but a good rule of thumb would be to create a new directory in
the **Views** directory for each controller. Then, name views by the method name. This makes them very easy find later
on. For example, a user's profile might be displayed in a controller named ``User``, and a method named ``profile``.
You might store the view file for this method in **/application/Views/User/Profile.php**.hh�h h!h"h?h$}q�(h&]h']h(]h)]h+]uh-K)h.hh]q�(h7X   Views are generally stored in qǅ�q�}q�(hX   Views are generally stored in hh�ubhJ)�q�}q�(hX   **/application/Views**h$}q�(h&]h']h(]h)]h+]uhh�h]q�h7X   /application/Viewsq΅�q�}q�(hU hh�ubah"hRubh7X�   , but can quickly become unwieldy if not organized in some fashion.
CodeIgniter does not enforce any type of organization, but a good rule of thumb would be to create a new directory in
the qх�q�}q�(hX�   , but can quickly become unwieldy if not organized in some fashion.
CodeIgniter does not enforce any type of organization, but a good rule of thumb would be to create a new directory in
the hh�ubhJ)�q�}q�(hX	   **Views**h$}q�(h&]h']h(]h)]h+]uhh�h]q�h7X   Viewsq؅�q�}q�(hU hh�ubah"hRubh7X�    directory for each controller. Then, name views by the method name. This makes them very easy find later
on. For example, a user's profile might be displayed in a controller named qۅ�q�}q�(hX�    directory for each controller. Then, name views by the method name. This makes them very easy find later
on. For example, a user's profile might be displayed in a controller named hh�ubh�)�q�}q�(hX   ``User``h$}q�(h&]h']h(]h)]h+]uhh�h]q�h7X   Userq⅁q�}q�(hU hh�ubah"h�ubh7X   , and a method named q允q�}q�(hX   , and a method named hh�ubh�)�q�}q�(hX   ``profile``h$}q�(h&]h']h(]h)]h+]uhh�h]q�h7X   profileq셁q�}q�(hU hh�ubah"h�ubh7X3   .
You might store the view file for this method in qq�}q�(hX3   .
You might store the view file for this method in hh�ubhJ)�q�}q�(hX'   **/application/Views/User/Profile.php**h$}q�(h&]h']h(]h)]h+]uhh�h]q�h7X#   /application/Views/User/Profile.phpq���q�}q�(hU hh�ubah"hRubh7X   .��q�}q�(hX   .hh�ubeubh;)�q�}q�(hX�   That type of organization works great as a base habit to get into. At times you might need to organize it differently.
That's not a problem. As long as CodeIgniter can find the file, it can display it.q�hh�h h!h"h?h$}q�(h&]h']h(]h)]h+]uh-K/h.hh]q�h7X�   That type of organization works great as a base habit to get into. At times you might need to organize it differently.
That's not a problem. As long as CodeIgniter can find the file, it can display it.r   ��r  }r  (hh�hh�ubaubh;)�r  }r  (hX1   :doc:`Find out more about views </general/views>`r  hh�h h!h"h?h$}r  (h&]h']h(]h)]h+]uh-K2h.hh]r  csphinx.addnodes
pending_xref
r  )�r	  }r
  (hj  hj  h h!h"Upending_xrefr  h$}r  (UreftypeX   docr  Urefwarnr  �U	reftargetr  X   /general/viewsU	refdomainU h)]h(]Urefexplicit�h&]h']h+]Urefdocr  X   concepts/mvcr  uh-K2h]r  cdocutils.nodes
inline
r  )�r  }r  (hj  h$}r  (h&]h']r  (Uxrefr  j  eh(]h)]h+]uhj	  h]r  h7X   Find out more about viewsr  ��r  }r  (hU hj  ubah"Uinliner  ubaubaubeubh)�r  }r  (hU hh�h h!h"h#h$}r   (h&]h']h(]h)]r!  hah+]r"  hauh-K6h.hh]r#  (h0)�r$  }r%  (hX   Modelsr&  hj  h h!h"h4h$}r'  (h&]h']h(]h)]h+]uh-K6h.hh]r(  h7X   Modelsr)  ��r*  }r+  (hj&  hj$  ubaubh;)�r,  }r-  (hXF  A model's job is to maintain a single type of data for the application. This might be users, blog posts, transactions, etc.
In this case, the model's job has two parts: enforce business rules on the data as it is pulled from, or put into, the
database; and handle the actual saving and retrieval of the data from the database.r.  hj  h h!h"h?h$}r/  (h&]h']h(]h)]h+]uh-K8h.hh]r0  h7XF  A model's job is to maintain a single type of data for the application. This might be users, blog posts, transactions, etc.
In this case, the model's job has two parts: enforce business rules on the data as it is pulled from, or put into, the
database; and handle the actual saving and retrieval of the data from the database.r1  ��r2  }r3  (hj.  hj,  ubaubh;)�r4  }r5  (hX�  For many developers, the confusion comes in when determining what business rules are enforced. It simply means that
any restrictions or requirements on the data is handled by the model. This might include normalizing raw data before
it's saved to meet company standards, or formatting a column in a certain way before handing it to the controller.
By keeping these business requirements in the model, you won't repeat code throughout several controllers and accidentally
miss updating an area.r6  hj  h h!h"h?h$}r7  (h&]h']h(]h)]h+]uh-K<h.hh]r8  h7X�  For many developers, the confusion comes in when determining what business rules are enforced. It simply means that
any restrictions or requirements on the data is handled by the model. This might include normalizing raw data before
it's saved to meet company standards, or formatting a column in a certain way before handing it to the controller.
By keeping these business requirements in the model, you won't repeat code throughout several controllers and accidentally
miss updating an area.r9  ��r:  }r;  (hj6  hj4  ubaubh;)�r<  }r=  (hXw   Models are typically stored in **/application/Models**, though they can use a namespace to be grouped however you need.r>  hj  h h!h"h?h$}r?  (h&]h']h(]h)]h+]uh-KBh.hh]r@  (h7X   Models are typically stored in rA  ��rB  }rC  (hX   Models are typically stored in hj<  ubhJ)�rD  }rE  (hX   **/application/Models**h$}rF  (h&]h']h(]h)]h+]uhj<  h]rG  h7X   /application/ModelsrH  ��rI  }rJ  (hU hjD  ubah"hRubh7XA   , though they can use a namespace to be grouped however you need.rK  ��rL  }rM  (hXA   , though they can use a namespace to be grouped however you need.hj<  ubeubh;)�rN  }rO  (hX3   :doc:`Find out more about models </database/model>`rP  hj  h h!h"h?h$}rQ  (h&]h']h(]h)]h+]uh-KDh.hh]rR  j  )�rS  }rT  (hjP  hjN  h h!h"j  h$}rU  (UreftypeX   docrV  j  �j  X   /database/modelU	refdomainU h)]h(]Urefexplicit�h&]h']h+]j  j  uh-KDh]rW  j  )�rX  }rY  (hjP  h$}rZ  (h&]h']r[  (j  jV  eh(]h)]h+]uhjS  h]r\  h7X   Find out more about modelsr]  ��r^  }r_  (hU hjX  ubah"j  ubaubaubeubh)�r`  }ra  (hU hh�h h!h"h#h$}rb  (h&]h']h(]h)]rc  hah+]rd  h	auh-KHh.hh]re  (h0)�rf  }rg  (hX   Controllersrh  hj`  h h!h"h4h$}ri  (h&]h']h(]h)]h+]uh-KHh.hh]rj  h7X   Controllersrk  ��rl  }rm  (hjh  hjf  ubaubh;)�rn  }ro  (hX�  Controllers have a couple of different roles to play. The most obvious one is that they receive input from the user and
then determine what to do with it. This often involves passing the data to a model to save it, or requesting data from
the model that is then passed on to the view to be displayed. This also includes loading up other utility classes,
if needed, to handle specialized tasks that is outside of the purview of the model.rp  hj`  h h!h"h?h$}rq  (h&]h']h(]h)]h+]uh-KJh.hh]rr  h7X�  Controllers have a couple of different roles to play. The most obvious one is that they receive input from the user and
then determine what to do with it. This often involves passing the data to a model to save it, or requesting data from
the model that is then passed on to the view to be displayed. This also includes loading up other utility classes,
if needed, to handle specialized tasks that is outside of the purview of the model.rs  ��rt  }ru  (hjp  hjn  ubaubh;)�rv  }rw  (hX)  The other responsibility of the controller is to handles everything that pertains to HTTP requests - redirects,
authentication, web safety, encoding, etc. In short, the controller is where you make sure that people are allowed to
be there, and they get the data they need in a format they can use.rx  hj`  h h!h"h?h$}ry  (h&]h']h(]h)]h+]uh-KOh.hh]rz  h7X)  The other responsibility of the controller is to handles everything that pertains to HTTP requests - redirects,
authentication, web safety, encoding, etc. In short, the controller is where you make sure that people are allowed to
be there, and they get the data they need in a format they can use.r{  ��r|  }r}  (hjx  hjv  ubaubh;)�r~  }r  (hX�   Controllers are typically stored in **/application/Controllers**, though they can use a namespace to be grouped however
you need.hj`  h h!h"h?h$}r�  (h&]h']h(]h)]h+]uh-KSh.hh]r�  (h7X$   Controllers are typically stored in r�  ��r�  }r�  (hX$   Controllers are typically stored in hj~  ubhJ)�r�  }r�  (hX   **/application/Controllers**h$}r�  (h&]h']h(]h)]h+]uhj~  h]r�  h7X   /application/Controllersr�  ��r�  }r�  (hU hj�  ubah"hRubh7XA   , though they can use a namespace to be grouped however
you need.r�  ��r�  }r�  (hXA   , though they can use a namespace to be grouped however
you need.hj~  ubeubh;)�r�  }r�  (hX=   :doc:`Find out more about controllers </general/controllers>`r�  hj`  h h!h"h?h$}r�  (h&]h']h(]h)]h+]uh-KVh.hh]r�  j  )�r�  }r�  (hj�  hj�  h h!h"j  h$}r�  (UreftypeX   docr�  j  �j  X   /general/controllersU	refdomainU h)]h(]Urefexplicit�h&]h']h+]j  j  uh-KVh]r�  j  )�r�  }r�  (hj�  h$}r�  (h&]h']r�  (j  j�  eh(]h)]h+]uhj�  h]r�  h7X   Find out more about controllersr�  ��r�  }r�  (hU hj�  ubah"j  ubaubaubeubeubeubahU Utransformerr�  NUfootnote_refsr�  }r�  Urefnamesr�  }r�  Usymbol_footnotesr�  ]r�  Uautofootnote_refsr�  ]r�  Usymbol_footnote_refsr�  ]r�  U	citationsr�  ]r�  h.hUcurrent_liner�  NUtransform_messagesr�  ]r�  Ureporterr�  NUid_startr�  KUautofootnotesr�  ]r�  Ucitation_refsr�  }r�  Uindirect_targetsr�  ]r�  Usettingsr�  (cdocutils.frontend
Values
r�  or�  }r�  (Ufootnote_backlinksr�  KUrecord_dependenciesr�  NUrfc_base_urlr�  Uhttps://tools.ietf.org/html/r�  U	tracebackr�  �Upep_referencesr�  NUstrip_commentsr�  NUtoc_backlinksr�  Uentryr�  Ulanguage_coder�  Uenr�  U	datestampr�  NUreport_levelr�  KU_destinationr�  NU
halt_levelr�  KUstrip_classesr�  Nh4NUerror_encoding_error_handlerr�  Ubackslashreplacer�  Udebugr�  NUembed_stylesheetr�  �Uoutput_encoding_error_handlerr�  Ustrictr�  Usectnum_xformr�  KUdump_transformsr�  NUdocinfo_xformr�  KUwarning_streamr�  NUpep_file_url_templater�  Upep-%04dr�  Uexit_status_levelr�  KUconfigr�  NUstrict_visitorr�  NUcloak_email_addressesr�  �Utrim_footnote_reference_spacer�  �Uenvr�  NUdump_pseudo_xmlr�  NUexpose_internalsr�  NUsectsubtitle_xformr�  �Usource_linkr�  NUrfc_referencesr�  NUoutput_encodingr�  Uutf-8r�  U
source_urlr�  NUinput_encodingr�  U	utf-8-sigr�  U_disable_configr�  NU	id_prefixr�  U Ucharacter_level_inline_markupr�  �U	tab_widthr�  KUerror_encodingr�  Ucp866r�  U_sourcer�  h!Ugettext_compactr�  �U	generatorr�  NUdump_internalsr�  NUsmart_quotesr�  �Upep_base_urlr�  U https://www.python.org/dev/peps/r�  Usyntax_highlightr�  Ulongr�  Uinput_encoding_error_handlerr�  j�  Uauto_id_prefixr�  Uidr�  Udoctitle_xformr�  �Ustrip_elements_with_classesr�  NU_config_filesr�  ]Ufile_insertion_enabledr�  �Uraw_enabledr�  KUdump_settingsr   NubUsymbol_footnote_startr  K h)}r  (hj  hj`  hh�hh�hhuUsubstitution_namesr  }r  h"h.h$}r  (h&]h)]h(]Usourceh!h']h+]uU	footnotesr  ]r  Urefidsr  }r	  ub.