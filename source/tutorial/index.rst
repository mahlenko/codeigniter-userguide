#######
Учебник
#######

Этот учебник предназначен, чтобы познакомить вас с фреймворком CodeIgniter 4
и основными принципами архитектуры MVC. Он расскажет вам, как
построить базовое приложение шаг за шагом.

В этом уроке Вы узнаете как создать **базовый новостной сайт**.
Вы научитесь писать код, который будет загружать статические страницы.
Далее Вы создадите раздел новостей, который читает и показывает новости
из базы данных. И наконец, добавите форму для создания новостей в базе данных.

Эти уроки в первую очередь ориентированы на:

-  Основы Модель-Вид-Контроллер (Model-View-Controller)
-  Основы маршрутизации (Routing)
-  Проверка формы (Form validation)
-  Выполнение основных запросов к базе данных используя "Query Builder"

Весь учебник разделен на несколько частей, каждая из которых объясняет небольшую
часть функциональности фреймворка CodeIgniter. Вам предстоит изучить следующие страницы:

-  Раздел введение, который дает Вам понять чего ожидать.
-  :doc:`Статические страницы <static_pages>`, научат основам использования контроллеров,
   представлений (вид) и маршрутизации.
-  :doc:`Раздел новостей <news_section>`, начнете использовать модели и изучите основные
   операции для работы с базой данных.
-  :doc:`Создание новостей <create_news_items>`, более продвинутые операции с базой данных
   и проверка (валидация) форм.
-  :doc:`Заключение <conclusion>`, в котором даны указания к дальнейшему изучению и ссылки на другие ресурсы.

Наслаждайтесь изучением фреймворка CodeIgniter.

.. toctree::
	:hidden:
	:titlesonly:

	static_pages
	news_section
	create_news_items
	conclusion