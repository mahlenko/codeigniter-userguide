######################################
Руководство пользователя CodeIgniter 4
######################################

- :doc:`License Agreement <license>`
- :doc:`Change Log <changelog>`

.. contents::
   :local:
   :depth: 2

****************
Добро пожаловать
****************

.. toctree::
	:includehidden:
        :titlesonly:

        intro/index

*********
Установка
*********

.. toctree::
	:includehidden:
	:maxdepth: 2
	:titlesonly:

	installation/index

*******
Учебник
*******

.. toctree::
	:includehidden:
	:titlesonly:

    	tutorial/index

*******************
Обзор CodeIgniter 4
*******************
.. toctree::
   :titlesonly:

   concepts/index

*************
Основные темы
*************

.. toctree::
	:titlesonly:

	general/index

**********
Библиотеки
**********

.. toctree::
	:titlesonly:

	libraries/index

***********
База данных
***********

.. toctree::
	:titlesonly:

	database/index

*******************
Помощники (Хелперы)
*******************

.. toctree::
	:titlesonly:

	helpers/index


*********************************
Содействия в развитии CodeIgniter
*********************************

.. toctree::
	:titlesonly:

   	contributing/index